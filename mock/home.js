const HomeData = [
    {
        title: '',
        logo: '',
        description: '',
        href: '',
    },
];

export default {
    'GET /api/homeData': HomeData,
};
